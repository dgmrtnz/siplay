import React from 'react'; 
import ReactDOM from 'react-dom'; 
import ReactTestUtils from 'react-dom/test-utils'; 
import _ from 'lodash';

import TaskForm from '../../src/tasks/components/TaskForm';

define((require) => {

	class Wrapper extends React.Component {
		render() { 
			return this.props.children
		}
	}

	describe('TaskForm', () => {
		const props = {
			id: 'TASK 01',
			title: 'My task'
		};

		const handlers = {
			handleAdd: () => true,
			handleUpdate: () => true
		};

		beforeAll(() => {
			spyOn(handlers, 'handleUpdate');
			spyOn(handlers, 'handleAdd');
		});

		const render = (_props) => {
			const component = ReactTestUtils.renderIntoDocument(
				<Wrapper>
					<TaskForm 
						{...handlers}
						{..._props}
					/>
				</Wrapper>
			);
			const container = ReactDOM.findDOMNode(component);

			return {component, container};
		};

		it('renders an input element', () => {
			const {container} = render();
			expect(container.querySelector('input')).not.toBeNull();
		});

		it('renders within a form group if adding', () => {
			const {container} = render();
			expect(container.classList.contains('form-group')).toBeTruthy();
		});

		it('renders control plain text if editing', () => {
			const {container} = render(props);
			const input = container.querySelector('input');
			expect(container.classList.contains('form-group')).toBeFalsy();
			expect(input.classList.contains('form-control-plaintext')).toBeTruthy();
		});

		it('should contain the title if editing', () => {
			const {container} = render(props);
			const input = container.querySelector('input');
			expect(input.value).toEqual(props.title);
		});

		it('should call handle add when new', () => {
			const {container} = render();
			const input = container.querySelector(`input`);
			expect(input).not.toBeNull();

			input.value = props.title;
			ReactTestUtils.Simulate.change(input);
			ReactTestUtils.Simulate.keyDown(input, {keyCode: 13});

			expect(handlers.handleAdd).toHaveBeenCalledWith(props.title);
		});

		it('should call handle update when editing', () => {
			const title = 'New Value';
			const {container} = render(props);
			const input = container.querySelector(`input[data-id="${props.id}"]`);
			expect(input).not.toBeNull();

			input.value = title;
			ReactTestUtils.Simulate.change(input);
			ReactTestUtils.Simulate.keyDown(input, {keyCode: 13});

			expect(handlers.handleUpdate).toHaveBeenCalledWith(props.id, title);
		});

	});

});