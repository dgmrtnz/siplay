import React from 'react'; 
import ReactDOM from 'react-dom'; 
import ReactTestUtils from 'react-dom/test-utils'; 
import _ from 'lodash';

import TaskProgress from '../../src/tasks/components/TaskProgress';

define((require) => {

	class Wrapper extends React.Component {
		render() { 
			return this.props.children
		}
	}

	describe('TaskProgress', () => {

		const render = (_props) => {
			const component = ReactTestUtils.renderIntoDocument(
				<Wrapper>
					<TaskProgress 
						{..._props}
					/>
				</Wrapper>
			);
			const container = ReactDOM.findDOMNode(component);

			return {component, container};
		};

		it('shouldn`t render if there are no tasks', () => {
			const {container} = render();
			expect(container.classList.contains('progress')).toBeFalsy();
		});

		it('renders a progress bar container', () => {
			const {container} = render({tasks: [{id: '1', title: 'task - 1', prority: 1}]});
			expect(container.querySelector('.progress')).not.toBeNull();
		});

		it('renders a progress bar', () => {
			const {container} = render({tasks: [{id: '1', title: 'task - 1', prority: 1}]});
			expect(container.querySelector('.progress-bar')).not.toBeNull();
		});

		it('renders a bar with 0%', () => {
			const props = {
				tasks: [
					{id: '1', title: 'task - 1', prority: 1},
					{id: '2', title: 'task - 2', prority: 2},
					{id: '3', title: 'task - 3', prority: 3},
					{id: '4', title: 'task - 4', prority: 4},
					{id: '5', title: 'task - 5', prority: 5}
				]
			};

			const {container} = render(props);
			expect(container.textContent).toBe('0%');

			const progress = container.querySelector('.progress-bar');
			expect(progress.style.width).toBe('0%');
		});

		it('renders a bar with 50%', () => {
			const props = {
				tasks: [
					{id: '1', title: 'task - 1', prority: 1, completed: true},
					{id: '2', title: 'task - 2', prority: 2, completed: true},
					{id: '3', title: 'task - 3', prority: 3},
					{id: '4', title: 'task - 4', prority: 4}
				]
			};

			const {container} = render(props);
			expect(container.textContent).toBe('50%');

			const progress = container.querySelector('.progress-bar');
			expect(progress.style.width).toBe('50%');
		});

	});

});