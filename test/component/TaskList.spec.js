import React from 'react'; 
import ReactDOM from 'react-dom'; 
import ReactTestUtils from 'react-dom/test-utils'; 
import _ from 'lodash';

import TaskList from '../../src/tasks/components/TaskList';

define((require) => {

	class Wrapper extends React.Component {
		render() { 
			return this.props.children
		}
	}

	describe('TaskList', () => {
		const props = {};
		const handlers = {
			handleLoad: () => true
		};

		beforeAll(() => {
			spyOn(handlers, 'handleLoad');
		});

		const render = (_props = props) => {
			const component = ReactTestUtils.renderIntoDocument(
				<TaskList 
					{..._props}
					{...handlers}
				/>
			);
			const container = ReactDOM.findDOMNode(component);

			return {component, container};
		};

		it('renders a list tag', () => {
			const {container} = render({tasks: [{id: '1', title: 'task', prority: 1}]});
			expect(container.querySelector('ul')).not.toBeNull();
		});

		it('renders a loading message if fetching', () => {
			const {container} = render({isFetching: true});
			expect(container.querySelector('p').textContent).toBe('Loading...');
		});

		it('should call load on startup', () => {
			const {container} = render();
			expect(handlers.handleLoad).toHaveBeenCalled();
		});

		it('renders an empty message when there are no items', () => {
			const {container} = render({});
			expect(container.querySelector('p').textContent).toBe('Enter task title and press enter key to add...');
		});

		it('renders a list with five elements', () => {
			const props = {
				tasks: [
					{id: '1', title: 'task - 1', prority: 1},
					{id: '2', title: 'task - 2', prority: 2},
					{id: '3', title: 'task - 3', prority: 3},
					{id: '4', title: 'task - 4', prority: 4},
					{id: '5', title: 'task - 5', prority: 5}
				]
			};

			const {container} = render(props);
			const lis = container.querySelectorAll('ul li');

			expect(lis.length).toBe(5);
			expect(lis[3].textContent).toBe(props.tasks[3].title);
		});
	});

});