import React from 'react'; 
import ReactDOM from 'react-dom'; 
import ReactTestUtils from 'react-dom/test-utils'; 
import _ from 'lodash';

import TaskItem from '../../src/tasks/components/TaskItem';

define((require) => {

	class Wrapper extends React.Component {
		render() { 
			return this.props.children
		}
	}

	describe('TaskItem', () => {
		const props = {
			id: 'TASK 01',
			title: 'My task',
			completed: false,
			priority: 1,
			editing: false
		};

		const handlers = {
			handleToggle: () => true,
			handleToggleEdit: () => true,
			handleUpdate: () => true,
			handleRemove: () => true
		};

		beforeAll(() => {
			spyOn(handlers, 'handleToggle');
			spyOn(handlers, 'handleToggleEdit');
			spyOn(handlers, 'handleUpdate');
			spyOn(handlers, 'handleRemove');
		});

		const render = (_props = props) => {
			const component = ReactTestUtils.renderIntoDocument(
				<Wrapper>
					<TaskItem 
						{...handlers}
						{..._props}
					/>
				</Wrapper>
			);
			const container = ReactDOM.findDOMNode(component);

			return {component, container};
		};

		it('renders a li element', () => {
			const {container} = render();
			expect(container.tagName.toLowerCase()).toEqual('li');
		});

		it('should display a task title', () => {
			const {container} = render();
			expect(container.textContent).toEqual(props.title);
		});

		it('should contain the priority as data attribute', () => {
			const {container} = render();
			expect(container.dataset.priority).not.toBeNull();
			expect(parseInt(container.dataset.priority)).toEqual(props.priority);
		});

		it('should have check if completed', () => {
			const {container} = render(_.extend({}, props, {completed: true}));
			const check = container.querySelector('i.fa-check');
			expect(check).not.toBeNull();
		});

		it('should allow to toggle edit mode', () => {
			const {container} = render(_.extend({}, props, {completed: true}));
			const editBtn = container.querySelector('i.fa-pencil');
			expect(editBtn).toBeDefined();
			expect(editBtn).not.toBeNull();

			ReactTestUtils.Simulate.click(editBtn);
			expect(handlers.handleToggleEdit).toHaveBeenCalledWith(props.id);
		});

		it('should render an input when in edit mode', () => {
			const title = 'New Value';
			const {container} = render(_.extend({}, props, {editing: true}));
			const input = container.querySelector(`input[data-id="${props.id}"]`);
			expect(input).not.toBeNull();

			input.value = title;
			ReactTestUtils.Simulate.change(input);
			ReactTestUtils.Simulate.keyDown(input, {keyCode: 13});

			expect(handlers.handleUpdate).toHaveBeenCalledWith(props.id, title);
		});

		it('should allow to be removed', () => {
			const {container} = render(_.extend({}, props, {completed: true}));
			const removeBtn = container.querySelector('i.fa-trash');
			expect(removeBtn).not.toBeNull();

			ReactTestUtils.Simulate.click(removeBtn);
			expect(handlers.handleRemove).toHaveBeenCalledWith(props.id);
		});
	});

});