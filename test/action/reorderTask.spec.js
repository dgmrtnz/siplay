import React from 'react'; 
import ReactDOM from 'react-dom'; 
import ReactTestUtils from 'react-dom/test-utils'; 
import _ from 'lodash';

import {REQUEST_REORDER_TASK, RECEIVE_REORDER_TASK} from '../../src/tasks/store/actions';

import {requestReorderTaskAction, receiveReorderTaskAction} from '../../src/tasks/store/actions/reorderTaskActions';

define((require) => {
	describe('ReorderTaskActions', () => {
		it('should create an action to trigger task reorder', () => {
			const id = 10;
			const priority = 1000;
			const req = requestReorderTaskAction(id, priority);
			
			expect(req.type).toEqual(REQUEST_REORDER_TASK);
			expect(req.payload).toBeDefined();
			expect(req.payload).toEqual({id, priority});
		});

		it('should create an action to receive the reordered tasks', () => {
			const tasks = {id: 1, title: 'My Title', priority: 1000};
			const req = receiveReorderTaskAction(tasks);
			
			expect(req.type).toEqual(RECEIVE_REORDER_TASK);
			expect(req.payload).toBeDefined();
			expect(req.payload).toEqual(tasks);
		});

	});
});