import React from 'react'; 
import ReactDOM from 'react-dom'; 
import ReactTestUtils from 'react-dom/test-utils'; 
import _ from 'lodash';

import {REQUEST_TOGGLE_EDIT_TASK} from '../../src/tasks/store/actions';

import {toggleEditTaskAction} from '../../src/tasks/store/actions/toggleEditTaskActions';

define((require) => {
	describe('ReorderTaskActions', () => {
		it('should create an action to trigger task toggleEdit', () => {
			const id = 10;
			const req = toggleEditTaskAction(id);
			
			expect(req.type).toEqual(REQUEST_TOGGLE_EDIT_TASK);
			expect(req.payload).toBeDefined();
			expect(req.payload).toEqual({id});
		});
	});
});