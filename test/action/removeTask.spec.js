import React from 'react'; 
import ReactDOM from 'react-dom'; 
import ReactTestUtils from 'react-dom/test-utils'; 
import _ from 'lodash';

import {REQUEST_REMOVE_TASK, RECEIVE_REMOVE_TASK} from '../../src/tasks/store/actions';

import {requestRemoveTaskAction, receiveRemoveTaskAction} from '../../src/tasks/store/actions/removeTaskActions';

define((require) => {
	describe('RemoveTaskActions', () => {
		it('should create an action to trigger tasks removing', () => {
			const id = 10;
			const req = requestRemoveTaskAction(id);
			
			expect(req.type).toEqual(REQUEST_REMOVE_TASK);
			expect(req.payload).toBeDefined();
			expect(req.payload).toEqual({id});
		});

		it('should create an action to receive the removed tasks', () => {
			const tasks = {id: 1, title: 'My Title'};
			const req = receiveRemoveTaskAction(tasks);
			
			expect(req.type).toEqual(RECEIVE_REMOVE_TASK);
			expect(req.payload).toBeDefined();
			expect(req.payload).toEqual(tasks);
		});

	});
});