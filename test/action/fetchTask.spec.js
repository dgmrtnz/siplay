import React from 'react'; 
import ReactDOM from 'react-dom'; 
import ReactTestUtils from 'react-dom/test-utils'; 
import _ from 'lodash';

import {REQUEST_FETCH_TASK, RECEIVE_FETCH_TASK} from '../../src/tasks/store/actions';

import {requestFetchTaskAction, receiveFetchTaskAction} from '../../src/tasks/store/actions/fetchTaskActions';

define((require) => {
	describe('FetchTaskActions', () => {
		it('should create an action to trigger tasks fetching', () => {
			const req = requestFetchTaskAction();
			
			expect(req.type).toEqual(REQUEST_FETCH_TASK);
			expect(req.payload).not.toBeDefined();
		});

		it('should create an action to receive the fetched tasks', () => {
			const tasks = [{id: 1, title: 'My Title'}];
			const req = receiveFetchTaskAction(tasks);
			
			expect(req.type).toEqual(RECEIVE_FETCH_TASK);
			expect(req.payload).toBeDefined();
			expect(req.payload).toEqual(tasks);
		});

	});
});