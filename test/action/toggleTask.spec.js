import React from 'react'; 
import ReactDOM from 'react-dom'; 
import ReactTestUtils from 'react-dom/test-utils'; 
import _ from 'lodash';

import {REQUEST_TOGGLE_TASK, RECEIVE_TOGGLE_TASK} from '../../src/tasks/store/actions';

import {requestToggleTaskAction, receiveToggleTaskAction} from '../../src/tasks/store/actions/toggleTaskActions';

define((require) => {
	describe('ToggleTaskActions', () => {
		it('should create an action to trigger task toggle', () => {
			const id = 10;
			const completed = true;
			const req = requestToggleTaskAction(id, completed);
			
			expect(req.type).toEqual(REQUEST_TOGGLE_TASK);
			expect(req.payload).toBeDefined();
			expect(req.payload).toEqual({id, completed});
		});

		it('should create an action to receive the toggleed tasks', () => {
			const tasks = {id: 1, title: 'My Title', completed: true};
			const req = receiveToggleTaskAction(tasks);
			
			expect(req.type).toEqual(RECEIVE_TOGGLE_TASK);
			expect(req.payload).toBeDefined();
			expect(req.payload).toEqual({id: 1, completed: true});
		});

	});
});