import React from 'react'; 
import ReactDOM from 'react-dom'; 
import ReactTestUtils from 'react-dom/test-utils'; 
import _ from 'lodash';

import {REQUEST_UPDATE_TASK, RECEIVE_UPDATE_TASK} from '../../src/tasks/store/actions';

import {requestUpdateTaskAction, receiveUpdateTaskAction} from '../../src/tasks/store/actions/updateTaskActions';

define((require) => {
	describe('UpdatedTaskActions', () => {
		const props = {
			id: 'TASK 01',
			title: 'My task',
			priority: 0
		};

		it('should create an action to trigger a task creation', () => {
			const req = requestUpdateTaskAction(props.id, props.title);
			
			expect(req.type).toEqual(REQUEST_UPDATE_TASK);
			expect(req.payload).toBeDefined();
			expect(req.payload).toEqual(_.pick(props, 'id', 'title'));
		});

		it('should create an action to receive the new task', () => {
			const req = receiveUpdateTaskAction(props);

			expect(req.type).toEqual(RECEIVE_UPDATE_TASK);
			expect(req.payload).toBeDefined();
			expect(req.payload).toEqual(_.pick(props, 'id', 'title'));
		});

	});
});