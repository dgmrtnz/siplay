import React from 'react'; 
import ReactDOM from 'react-dom'; 
import ReactTestUtils from 'react-dom/test-utils'; 
import _ from 'lodash';

import {REQUEST_POST_TASK, RECEIVE_POST_TASK} from '../../src/tasks/store/actions';

import {requestPostTaskAction, receivePostTaskAction} from '../../src/tasks/store/actions/addTaskActions';

define((require) => {
	describe('AddTaskActions', () => {
		const props = {
			title: 'My task',
			priority: 0
		};

		it('should create an action to trigger a task creation', () => {
			const req = requestPostTaskAction(props.title, props.priority);
			
			expect(req.type).toEqual(REQUEST_POST_TASK);
			expect(req.payload).toBeDefined();
			expect(req.payload).toEqual(_.extend({completed: false}, props));
		});

		it('should create an action to receive the new task', () => {
			const json = _.extend({id: 0, completed: false}, props);
			const req = receivePostTaskAction(json);

			expect(req.type).toEqual(RECEIVE_POST_TASK);
			expect(req.payload).toBeDefined();
			expect(req.payload).toEqual(json);
		});

	});
});