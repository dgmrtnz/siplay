import React from 'react';

const BASE_URL = '/api';
const REQUEST_BODY_METHODS = ['POST', 'PUT', 'PATCH'];

function hasRequestBody(method) {
    return REQUEST_BODY_METHODS.indexOf(method) >= 0;
}

function RequestBody(props) {
    if (!hasRequestBody(props.method)) {
        return null;
    }
    return (
        <div className="row">
            <div className="col">
                <div className="form-group">
                    <label>Data (JSON)</label>
                    <textarea className="form-control" name="data" placeholder='{ "foo": "bar" }' value={props.data} onChange={props.onChange} />
                </div>
            </div>
        </div>
    );
}

function Response(props) {
    if (!props.response) {
        return null;
    }

    return (
        <div className="row">
            <div className="col">
                <div className={'alert ' + (props.response.status >= 200 && props.response.status < 400 ? "alert-success" : "alert-danger") }>
                    <h2>{props.response.status}</h2>
                </div>
                <ResponseData data={props.response.data} />
            </div>
        </div>
    );
}

function ResponseData(props) {
    if (!props.data) {
        return null;
    }
    return (
        <pre>
            <code>{props.data}</code>
        </pre>
    );
}

export default class ApiDemo extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            method: 'GET'
        };
        this.handleChange = this.handleChange.bind(this);
        this.send = this.send.bind(this);
    }

    send(e) {
        console.log(this.state.data);

        fetch(this.state.url, {
            method: this.state.method,
            headers: {
                Authorization: this.state.authorization,
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: hasRequestBody(this.state.method) ? this.state.data : null
        })
            .then(
                response => response,
                response => response
            )
            .then(response => response.text()
                .then(data =>
                    this.setState({ response: { status: response.status, data }})));

        e.preventDefault();
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        }, () => {
            if (!this.state.path) {
                return;
            }
            this.setState({
                url: `${BASE_URL}${this.state.path.match(/^\//) ? '' : '/'}${this.state.path}`
            });
        });
    }

    render() {
        return (
            <div>
                <h1>Dev API Demo</h1>

                <form name="form" onSubmit={this.send}>

                    <div className="row">
                        <div className="col-2">
                            <div className="form-group">
                                <label htmlFor="request-method">Method</label>
                                <select id="request-method" name="method" className="form-control" required value={this.state.method} onChange={this.handleChange}>
                                    <option value="GET">GET</option>
                                    <option value="POST">POST</option>
                                    <option value="PUT">PUT</option>
                                    <option value="PATCH">PATCH</option>
                                    <option value="DELETE">DELETE</option>
                                </select>
                            </div>
                        </div>

                        <div className="col-8">
                            <div className="form-group">
                                <label htmlFor="request-path">Path</label>
                                <input id="request-path" name="path" className="form-control" required value={this.state.path} onChange={this.handleChange} placeholder="/example/resource" />
                                {this.state.url}
                            </div>
                        </div>

                        <div className="col-2">
                            <div className="form-group">
                                <label>&nbsp;</label>
                                <input className="btn btn-success form-control" type="submit" value="Send" />
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <label htmlFor="request-headers-authorization">Authorization (User)</label>
                                <input id="request-headers-authorization" name="authorization" type="text" className="form-control" required value={this.state.authorization} onChange={this.handleChange} placeholder="Your name here!" />
                            </div>
                        </div>
                    </div>

                    <RequestBody method={this.state.method} onChange={this.handleChange} />
                    <Response response={this.state.response} />
                </form>
            </div>
        );
    }
}