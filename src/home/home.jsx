import * as React from 'react';

import InstructionsComponent from '../instructions.component';

import './home.scss';

export default class Home extends React.Component {

    render() {
        return (
            <home>
                <InstructionsComponent />
            </home>
        )
    }
}