import React from 'react';
import {Provider} from 'react-redux';
import './tasks.scss';

import store from './store';

import TaskListContainer from './components/TaskListContainer';
import TaskProgressContainer from './components/TaskProgressContainer';
import TaskFormContainer from './components/TaskFormContainer';

export default class Tasks extends React.Component {

    /** Write unit tests as appropriate.
        - Display the list of tasks.
        - Use the Dev REST API to persist and query task data (see Dev API documentation).
        - Provide a field to add new tasks.
        - Allow tasks to be toggled as "done" or "not done".
        - "done" tasks must be styled with a strike through.
        - Allow the title of tasks to be edited.
        - Allow tasks to be deleted from the list.
        - Allow tasks to be reordered.
    **/

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <tasks>
                <Provider store={store}>
                    <div>
                        <h1>Tasks</h1>
                        <TaskFormContainer />
                        <TaskProgressContainer />
                        <TaskListContainer/>
                    </div>
                </Provider>
            </tasks>
        );
    }
}