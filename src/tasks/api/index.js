
export const getTask = () => {
    return fetch('/api/task',
        {
            method: 'GET',
            headers: {
                Authorization: 'diego',
                Accept: 'application/json',
                'Content-Type': 'application/json'
            }
        });
};

export const postTask = task => {
    return fetch('/api/task',
        {
            method: 'POST',
            headers: {
                Authorization: 'diego',
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(task)
        });
};

export const patchTask = (id, task) => {
    return fetch(`/api/task/${id}`,
        {
            method: 'PATCH',
            headers: {
                Authorization: 'diego',
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(task)
        });

};

export const removeTask = id => {
    return fetch(`/api/task/${id}`,
        {
            method: 'DELETE',
            headers: {
                Authorization: 'diego',
                Accept: 'application/json',
                'Content-Type': 'application/json'
            }
        });
};