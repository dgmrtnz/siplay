import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {fetchTaskAction} from '../store/actions/fetchTaskActions';
import {toggleTaskAction} from '../store/actions/toggleTaskActions';
import {toggleEditTaskAction} from '../store/actions/toggleEditTaskActions';
import {updateTaskAction} from '../store/actions/updateTaskActions';
import {removeTaskAction} from '../store/actions/removeTaskActions';
import {reorderTaskAction} from '../store/actions/reorderTaskActions';

import TaskList from './TaskList';

import _ from 'lodash';

const mapStateToProps = ({tasks, isPosting, isFetchin}) => ({tasks: _.sortBy(tasks, ['priority']), isPosting, isFetchin});

const mapDispatchToProps = (dispatch) => bindActionCreators(
	{
		handleLoad: fetchTaskAction,
		handleToggle: toggleTaskAction,
		handleToggleEdit: toggleEditTaskAction,
		handleUpdate: updateTaskAction,
		handleRemove: removeTaskAction,
		handleReorder: reorderTaskAction
	}, 
	dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(TaskList);
