import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {addTaskAction} from '../store/actions/addTaskActions';
import {updateTaskAction} from '../store/actions/updateTaskActions';

import TaskForm from './TaskForm';

const mapStateToProps = ({tasks}) => ({tasks});

const mapDispatchToProps = (dispatch) => bindActionCreators(
	{
		handleAdd: addTaskAction,
		handleUpdate: updateTaskAction
	}, 
	dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(TaskForm);
