import {connect} from 'react-redux';
import TaskProgress from './TaskProgress';

const mapStateToProps = ({tasks}) => ({tasks});
export default connect(mapStateToProps)(TaskProgress);
