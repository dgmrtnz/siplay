import React from 'react';
import TaskItem from './TaskItem';
import TaskProgress from './TaskProgress';
import './TaskList.scss';

export default class TaskList extends React.Component {
	constructor(props) {
		super(props);
		this.dragged;
		this.over;

		this.dragStart = this.dragStart.bind(this);
		this.dragEnd = this.dragEnd.bind(this);
		this.dragOver = this.dragOver.bind(this);
	}

	dragStart(e) {
		this.dragged = e.currentTarget;
		e.dataTransfer.setData('text/html', this.dragged);
	}

	dragEnd(e) {
		this.dragged.style.display = 'block';
		this.placeholder.style.display = 'none';
		this.dragged.parentNode.insertBefore(this.dragged, this.placeholder);
		this.dragged.parentNode.removeChild(this.placeholder);

		let prev = this.dragged.previousSibling ? this.dragged.previousSibling.dataset.priority : null;
		let next = this.dragged.nextSibling ? this.dragged.nextSibling.dataset.priority : null;

		prev = parseInt(prev || 0);
		next = parseInt(next || prev * 2);

		let priority = (next - prev) / 2 + prev;


		this.props.handleReorder(this.dragged.dataset.id, priority);
	}

	dragOver(e) {
		e.preventDefault();
		
		if(! e.target.classList.contains('list-group-item')) return false;

		this.dragged.style.display = 'none';
		this.placeholder.style.display = 'block';
		e.dataTransfer.dropEffect = 'move';

		this.over = e.target;
		e.target.parentNode.insertBefore(this.placeholder, e.target);
		return false;
	}

	componentDidMount() {
		this.props.handleLoad();
	}

	renderList () {
		return this.props.tasks.map((task, i) => (<TaskItem key={task.id} {...this.props} {...task} onDragStart={this.dragStart} onDragEnd={this.dragEnd}/>));
	}

	render() {
		const {tasks, isFetching} = this.props;

		return (
			<div>
			{tasks && tasks.length ? (
				<div>
					<ul className='list-group' onDragOver={this.dragOver}>
						{this.renderList()}

					</ul>
					<li className='placeholder list-group-item' ref={el => this.placeholder = el}>Drop it here</li>
				</div>
			) : (
				<p>{`${isFetching ? 'Loading...' : 'Enter task title and press enter key to add...'}`}</p>
			)}
			</div>
		);

	}
};