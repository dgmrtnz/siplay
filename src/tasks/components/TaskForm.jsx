import React from 'react';
export default function TaskForm({id, title, handleAdd, handleUpdate}) {
	const handleOnEnter = event => {
		let code = event.keyCode ? event.keyCode : event.which;
		const title = event.target.value;
		const {id} = event.target.dataset;

		if(code === 13 && title.trim()) {
			if(!id) {
				handleAdd(title);
				event.target.value = '';
			}else{
				handleUpdate(id, title);
			}
		}
	}

	return (
		<div className={id === undefined ? 'form-group' : ''}>
			<input 
				data-id={id}
				className={id === undefined  ? 'form-control form-control-lg' : 'form-control-plaintext'}
				type='text'
				onKeyDown={handleOnEnter}
				placeholder='Enter task title...'
				defaultValue={title}
			/>
		</div>
	);
}