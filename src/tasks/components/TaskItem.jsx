import React from 'react';
import TaskForm from './TaskForm';

import './TaskItem.scss';

export default function TaskItem({id, title, completed, priority, editing, handleToggle, handleToggleEdit, handleUpdate, handleRemove, onDragStart, onDragEnd}){
	return (
		<li
			data-id={id}
			data-completed={completed}
			data-priority={priority}
			onClick={(e) => !editing && handleToggle(id, !completed)}
			className='task-item list-group-item'
			title='Click to complete'
			draggable
			onDragStart={onDragStart}
			onDragEnd={onDragEnd}
			>

			{!editing ? (
				<div>
					{title}
					{completed ? (<i className='fa fa-fw fa-check' />) : ''}
					<div className='task-item-actions'>
						<i
							className='fa fa-fw fa-pencil'
							onClick={ e => e.stopPropagation() || handleToggleEdit(id) }
						/>
						<i
							className='fa fa-fw fa-trash'
							onClick={ e => e.stopPropagation() || handleRemove(id) }
						/>
					</div>
				</div>
			) : (
				<TaskForm 
					id={id}
					title={title}
					handleUpdate={handleUpdate}
				/>
			)}
		</li>
	);
};