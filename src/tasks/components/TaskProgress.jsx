import React from 'react';
export default function TaskProgress({tasks}) {

	const completed = tasks ? tasks.reduce((memo, t) => memo + (t.completed ? 1 : 0), 0) : 0;
	const outOf = tasks && tasks.length ? tasks.length : 1;
	const progress = Math.round(completed / outOf * 100);
	const percent = `${progress}%`;
	return (
		<div>
		{tasks && tasks.length ? (
			<div className='progress my-2' style={{background: 'rgba(0,0,0,.1)'}}>
				<div className='progress-bar bg-info' role='progressbar' style={{width: percent}}>{percent}</div>
			</div>
		) : ''}
		</div>
	);
};
