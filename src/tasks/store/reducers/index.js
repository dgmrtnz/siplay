import _ from 'lodash';
import { combineReducers } from 'redux'

import {
	REQUEST_FETCH_TASK,
	RECEIVE_FETCH_TASK,

	REQUEST_POST_TASK,
	RECEIVE_POST_TASK,

	REQUEST_UPDATE_TASK,
	RECEIVE_UPDATE_TASK,

	REQUEST_REMOVE_TASK,
	RECEIVE_REMOVE_TASK,

	REQUEST_TOGGLE_TASK,
	RECEIVE_TOGGLE_TASK,

	REQUEST_TOGGLE_EDIT_TASK,

	REQUEST_REORDER_TASK,
	RECEIVE_REORDER_TASK
} from '../actions';


const tasksReducer = (
	previousState = {
		isPosting: false,
		isFetchin: true,
		tasks: []
	},
	action
) => {
	const {type, payload} = action;

	switch(type) {
		//FETCH
		case REQUEST_FETCH_TASK:
			return Object.assign({}, previousState, {
				isFetchin: true
			});

		case RECEIVE_FETCH_TASK:
			return Object.assign({}, previousState, {
				isFetchin: false,
				tasks: [...previousState.tasks, ...payload]
			});

		//Create Task
		case REQUEST_POST_TASK:
			return Object.assign({}, previousState, {
				isPosting: true
			});

		case RECEIVE_POST_TASK:
			return Object.assign({}, previousState, {
				isPosting: false,
				tasks: [...previousState.tasks, payload]
			});

		//UPDATE
		case REQUEST_UPDATE_TASK:
			//Remove the task from previousState
			return Object.assign({}, previousState, {
				isPosting: true,
				tasks: previousState.tasks.map(task => {
					if(task.id === payload.id) return Object.assign({}, task, {editing: false});
					else return task
				})
			});

		case RECEIVE_UPDATE_TASK:
			//Remove the task from previousState
			return Object.assign({}, previousState, {
				isPosting: true,
				tasks: previousState.tasks.map(task => {
					if(task.id == payload.id) return Object.assign({}, task, {editing: false}, payload);
					else return task
				})
			});

		//REMOVE
		case REQUEST_REMOVE_TASK:
			return Object.assign({}, previousState, {
				isPosting: true
			});

		case RECEIVE_REMOVE_TASK:

			//Remove the task from previousState
			return Object.assign({}, previousState, {
				isPosting: false,
				tasks: _.reject(previousState.tasks, task => task.id == payload.id)
			});

		//TOGGLE
		case REQUEST_TOGGLE_TASK:
			return Object.assign({}, previousState, {
				isPosting: true
			});

		case RECEIVE_TOGGLE_TASK:
			//Remove the task from previousState
			return Object.assign({}, previousState, {
				isPosting: false,
				tasks: previousState.tasks.map(task => {
					if(task.id === payload.id) return Object.assign({}, task, payload);
					else return task
				})
			});

		// TOGGLE EDIT 
		case REQUEST_TOGGLE_EDIT_TASK:
			//Remove the task from previousState
			return Object.assign({}, previousState, {
				isPosting: false,
				tasks: previousState.tasks.map(task => {
					if(task.id == payload.id) return Object.assign({}, task, {editing: true});
					else return task
				})
			});

		//REORDER
		case REQUEST_REORDER_TASK:
			return Object.assign({}, previousState, {
				isPosting: true
			});

		case RECEIVE_REORDER_TASK:
			//Remove the task from previousState
			return Object.assign({}, previousState, {
				isPosting: false,
				tasks: previousState.tasks.map(task => {
					if(task.id == payload.id) return Object.assign({}, task, payload);
					else return task
				})
			});
	}

	return previousState;
};


export default tasksReducer;