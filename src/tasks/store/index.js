import thunkMiddleware from 'redux-thunk';
import {dispatch, bindActionCreators, createStore, applyMiddleware} from 'redux';
import reducer from './reducers';

export default createStore(reducer, applyMiddleware(thunkMiddleware));