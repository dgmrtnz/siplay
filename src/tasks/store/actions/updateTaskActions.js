import {patchTask} from '../../api';

import {
  REQUEST_UPDATE_TASK,
  RECEIVE_UPDATE_TASK
} from './';

export const requestUpdateTaskAction = (id, title) => ({
	type: REQUEST_UPDATE_TASK,
	payload: {id, title}
});

export const receiveUpdateTaskAction = ({id, title}) => ({
    type: RECEIVE_UPDATE_TASK,
    payload: {id, title}
});

export const updateTaskAction = (id, title) => {
  return (dispatch) => {
    dispatch(requestUpdateTaskAction(id, title));

    return patchTask(id, {title})
      .then(
        response => response.json(),
        error => console.log('An error occured.', error)
      )
      .then( json => dispatch(receiveUpdateTaskAction(json)) );
  }
}