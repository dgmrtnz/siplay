import {getTask} from '../../api';

import {
  REQUEST_FETCH_TASK,
  RECEIVE_FETCH_TASK
} from './';

export const requestFetchTaskAction = () => ({
	type: REQUEST_FETCH_TASK
});

export const receiveFetchTaskAction = json => ({
    type: RECEIVE_FETCH_TASK,
    payload: json
});

export const fetchTaskAction = () => {
  return (dispatch) => {
    dispatch(requestFetchTaskAction());

    return getTask()
      .then(
        response => response.json(),
        error => console.log('An error occured.', error)
      )
      .then( json => dispatch(receiveFetchTaskAction(json)) );
  }
}