import {patchTask} from '../../api';

import {
  REQUEST_REORDER_TASK,
  RECEIVE_REORDER_TASK
} from './';

export const requestReorderTaskAction = (id, priority) => ({
	type: REQUEST_REORDER_TASK,
	payload: {id, priority}
});

export const receiveReorderTaskAction = json => ({
    type: RECEIVE_REORDER_TASK,
    payload: json
});

export const reorderTaskAction = (id, priority) => {
  return (dispatch) => {
    dispatch(requestReorderTaskAction(id, priority));

    return patchTask(id, {priority})
      .then(
        response => response.json(),
        error => console.log('An error occured.', error)
      )
      .then( json => dispatch(receiveReorderTaskAction(json)) );
  }
}