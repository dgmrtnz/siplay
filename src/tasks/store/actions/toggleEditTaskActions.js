import {
  REQUEST_TOGGLE_EDIT_TASK
} from './';

export const toggleEditTaskAction = (id) => ({
    type: REQUEST_TOGGLE_EDIT_TASK,
    payload: {id}
});
