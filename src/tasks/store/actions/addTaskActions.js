import {postTask} from '../../api';

import {
  REQUEST_POST_TASK,
  RECEIVE_POST_TASK
} from './';

export const requestPostTaskAction = (title, priority) => ({
	type: REQUEST_POST_TASK,
	payload: {title, priority, completed: false}
});

export const receivePostTaskAction = ({id, title, priority, completed}) => ({
    type: RECEIVE_POST_TASK,
    payload: {id, title, priority, completed}
});

export const addTaskAction = title => {
  return (dispatch) => {
    dispatch(requestPostTaskAction(title));
    const data = {title, completed: false, priority: new Date().getTime()};
    return postTask(data)
      .then(
        response => response.json(),
        error => console.log('An error occured.', error)
      )
      .then( json => dispatch(receivePostTaskAction(json)) );
  }
}