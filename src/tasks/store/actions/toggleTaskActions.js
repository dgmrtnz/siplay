import {patchTask} from '../../api';

import {
  REQUEST_TOGGLE_TASK,
  RECEIVE_TOGGLE_TASK
} from './';

export const requestToggleTaskAction = (id, completed) => ({
  type: REQUEST_TOGGLE_TASK,
  payload: {id, completed}
});

export const receiveToggleTaskAction = ({id, completed}) => ({
    type: RECEIVE_TOGGLE_TASK,
    payload: {id, completed}
});

export const toggleTaskAction = (id, completed) => {
  return (dispatch) => {
    dispatch(requestToggleTaskAction(id, completed));

    return patchTask(id, {completed})
      .then(
        response => response.json(),
        error => console.log('An error occured.', error)
      )
      .then( json => dispatch(receiveToggleTaskAction(json)) );
  }
}