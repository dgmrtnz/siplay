import {removeTask} from '../../api';
import {
  REQUEST_REMOVE_TASK,
  RECEIVE_REMOVE_TASK
} from './';

export const requestRemoveTaskAction = (id) => ({
	type: REQUEST_REMOVE_TASK,
	payload: {id}
});

export const receiveRemoveTaskAction = json => ({
    type: RECEIVE_REMOVE_TASK,
    payload: json
});

export const removeTaskAction = id => {
  return (dispatch) => {
    dispatch(requestRemoveTaskAction(id));

    return removeTask(id)
      .then(
        () => ({id}),
        error => console.log('An error occured.', error)
      )
      .then( json => dispatch(receiveRemoveTaskAction(json)) );
  }
}